var date = new Date();

function Clock () {
	return this;
}

Clock.prototype.getHours = function () {
	return date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
};

Clock.prototype.getMinutes = function () {
	return date.getMinutes() > 9 ? ':' + date.getMinutes() : ':0' + date.getMinutes();
};

Clock.prototype.getTime = function () {
	return this.getHours() + this.getMinutes();
};

Clock.prototype.normalize = function () {
	return this.getTime();
};

function FullClock () {
	return this;
}

extend(Clock, FullClock);

function extend (Parent, Child) {
	var Surrogate = function() {}
	
	Surrogate.prototype = Parent.prototype;

	Child.prototype = new Surrogate();
	Child.prototype.constructor = Child;
	Child.super = Parent.prototype;
}

FullClock.prototype.getSeconds = function () {
	return date.getSeconds() > 9 ? ':' + date.getSeconds() : ':0' + date.getSeconds();
};

FullClock.prototype.getTime = function () {
	return FullClock.super.getTime() + this.getSeconds();
};


var c = new Clock();
console.dir(c.getTime());