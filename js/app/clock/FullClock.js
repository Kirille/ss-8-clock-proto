var FullClock = (function () {
	var date = new Date();

	function FullClock () {
		return this;
	}
	
	extend(Clock, FullClock);

	FullClock.prototype.getSeconds = function () {
		return date.getSeconds() > 9 ? ':' + date.getSeconds() : ':0' + date.getSeconds();
	};

	FullClock.prototype.getTime = function () {
		return FullClock.super.getTime() + this.getSeconds();
	};

	return FullClock;
})();