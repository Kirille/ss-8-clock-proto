var Clock = (function () {
	var date = new Date();

	function Clock () {
		return this;
	}

	Clock.prototype.getHours = function () {
		return date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
	}

	Clock.prototype.getMinutes = function () {
		return date.getMinutes() > 9 ? ':' + date.getMinutes() : ':0' + date.getMinutes();
	}

	Clock.prototype.getTime = function () {
		return this.getHours() + this.getMinutes();
	}

	Clock.prototype.normalize = function () {
		return this.getTime();
	}

	return Clock;
})();