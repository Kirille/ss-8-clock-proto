document.addEventListener('DOMContentLoaded', init, false);

function init () {
	var id = document.getElementById('clock'),
		clock = new FullClock();

	id.innerHTML = clock.getTime();
}